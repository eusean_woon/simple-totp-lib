/*
 * Public API Surface of simple-totp-lib
 */

export * from './lib/simple-totp-lib.service';
export * from './lib/simple-totp-lib.component';
export * from './lib/simple-totp-lib.module';
