import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SimpleTotpLibComponent } from './simple-totp-lib.component';

describe('SimpleTotpLibComponent', () => {
  let component: SimpleTotpLibComponent;
  let fixture: ComponentFixture<SimpleTotpLibComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SimpleTotpLibComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SimpleTotpLibComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
