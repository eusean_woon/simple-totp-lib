import { TestBed } from '@angular/core/testing';

import { SimpleTotpLibService } from './simple-totp-lib.service';

describe('SimpleTotpLibService', () => {
  let service: SimpleTotpLibService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(SimpleTotpLibService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
