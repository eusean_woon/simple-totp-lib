import { Injectable } from '@angular/core';
import * as CryptoJS from 'crypto-js';

// Code copy-pasted from https://github.com/Authenticator-Extension/Authenticator

export enum OTPType {
  totp = 1,
  hotp,
  battle,
  steam,
  hex,
  hhex,
}

export enum OTPAlgorithm {
  SHA1 = 1,
  SHA256,
  SHA512,
}

@Injectable({
  providedIn: 'root',
})
export class SimpleTotpLibService {
  constructor() {}

  private static dec2hex(s: number): string {
    return (s < 15.5 ? '0' : '') + Math.round(s).toString(16);
  }

  private static hex2dec(s: string): number {
    return Number(`0x${s}`);
  }

  private static hex2str(hex: string) {
    let str = '';
    for (let i = 0; i < hex.length; i += 2) {
      str += String.fromCharCode(this.hex2dec(hex.substr(i, 2)));
    }
    return str;
  }

  private static leftpad(str: string, len: number, pad: string): string {
    if (len + 1 >= str.length) {
      str = new Array(len + 1 - str.length).join(pad) + str;
    }
    return str;
  }

  private static base32tohex(base32: string): string {
    const base32chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ234567';
    let bits = '';
    let hex = '';
    let padding = 0;

    for (let i = 0; i < base32.length; i++) {
      if (base32.charAt(i) === '=') {
        bits += '00000';
        padding++;
      } else {
        const val = base32chars.indexOf(base32.charAt(i).toUpperCase());
        bits += this.leftpad(val.toString(2), 5, '0');
      }
    }

    for (let i = 0; i + 4 <= bits.length; i += 4) {
      const chunk = bits.substr(i, 4);
      hex = hex + Number(`0b${chunk}`).toString(16);
    }

    switch (padding) {
      case 0:
        break;
      case 6:
        hex = hex.substr(0, hex.length - 8);
        break;
      case 4:
        hex = hex.substr(0, hex.length - 6);
        break;
      case 3:
        hex = hex.substr(0, hex.length - 4);
        break;
      case 1:
        hex = hex.substr(0, hex.length - 2);
        break;
      default:
        throw new Error('Invalid Base32 string');
    }

    return hex;
  }

  private static base26(num: number) {
    const chars = '23456789BCDFGHJKMNPQRTVWXY';
    let output = '';
    const len = 5;
    for (let i = 0; i < len; i++) {
      output += chars[num % chars.length];
      num = Math.floor(num / chars.length);
    }
    if (output.length < len) {
      output = new Array(len - output.length + 1).join(chars[0]) + output;
    }
    return output;
  }

  static generate(
    type: OTPType,
    secret: string,
    counter: number,
    period: number,
    len?: number,
    algorithm?: OTPAlgorithm
  ) {
    // Replace all whitespace with nothing.
    secret = secret.replace(/\s/g, '');

    // If length of code is not given then default to 6 digits.
    if (!len) {
      len = 6;
    }

    // For use in generating Steam TOTPs
    let b26 = false;

    let key: string;
    switch (type) {
      // If the type of OTP is time-based or event-based
      // Then convert the secret to base32
      case OTPType.totp:
      case OTPType.hotp:
        key = this.base32tohex(secret);
        break;
      case OTPType.hex:
      case OTPType.hhex:
        key = secret;
        break;
      case OTPType.battle:
        key = this.base32tohex(secret);
        len = 8;
        break;
      case OTPType.steam:
        key = this.base32tohex(secret);
        len = 10;
        b26 = true;
        break;
      default:
        key = this.base32tohex(secret);
    }

    if (!key) {
      throw new Error('Invalid secret key');
    }

    // If the OTP type is time-based, generate the counter
    // based on the given period.
    if (type !== OTPType.hotp && type !== OTPType.hhex) {
      let epoch = Math.round(new Date().getTime() / 1000.0);
      if (localStorage.offset) {
        epoch = epoch + Number(localStorage.offset);
      }
      counter = Math.floor(epoch / period);
    }

    // If the counter is not 16 in length, left-pad it with 0s.
    const time = this.leftpad(this.dec2hex(counter), 16, '0');

    // If the key length is odd-numbered
    if (key.length % 2 === 1) {
      // If the last char of the key is 0
      if (key.substr(-1) === '0') {
        // Remove the last char
        key = key.substr(0, key.length - 1);
      } else {
        // Else add 0 to the back of the key
        key += '0';
      }
    }

    // Generate the HMAC hash based on given algoritm option
    // and hex-ed values of the time and key values.
    let hmacObj: CryptoJS.lib.WordArray;
    switch (algorithm) {
      case OTPAlgorithm.SHA256:
        hmacObj = CryptoJS.HmacSHA256(
          CryptoJS.enc.Hex.parse(time),
          CryptoJS.enc.Hex.parse(key)
        );
        break;
      case OTPAlgorithm.SHA512:
        hmacObj = CryptoJS.HmacSHA512(
          CryptoJS.enc.Hex.parse(time),
          CryptoJS.enc.Hex.parse(key)
        );
        break;
      default:
        hmacObj = CryptoJS.HmacSHA1(
          CryptoJS.enc.Hex.parse(time),
          CryptoJS.enc.Hex.parse(key)
        );
        break;
    }

    // Convert the hash to hexstring
    const hmac = CryptoJS.enc.Hex.stringify(hmacObj);

    let offset = 0;

    // Get the offset to the last hex value of the HMAC hashstring
    // and convert it to decimal, then set offset to the value.
    offset = this.hex2dec(hmac.substring(hmac.length - 1));

    // Gets a substring of HMAC hash with length of 8 from the offset.
    // The value is then bitwise-AND-ed with 7fffffff to mask out the
    // sign bit to ensure that the value is positive.
    let otp =
      (this.hex2dec(hmac.substr(offset * 2, 8)) & this.hex2dec('7fffffff')) +
      '';

    if (b26) {
      return this.base26(Number(otp));
    }

    // If the length of the OTP is less than the length needed,
    if (otp.length < len) {
      // left-pad with zeroes the difference in length and append
      // the OTP to the end
      otp = new Array(len - otp.length + 1).join('0') + otp;
    }

    // Return the last `len` length chars from the OTP as string
    return otp.substr(otp.length - len, len).toString();
  }
}
