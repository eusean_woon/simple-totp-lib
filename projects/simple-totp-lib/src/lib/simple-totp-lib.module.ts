import { NgModule } from '@angular/core';
import { SimpleTotpLibComponent } from './simple-totp-lib.component';



@NgModule({
  declarations: [
    SimpleTotpLibComponent
  ],
  imports: [
  ],
  exports: [
    SimpleTotpLibComponent
  ]
})
export class SimpleTotpLibModule { }
